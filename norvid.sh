#!/bin/bash

slab_exec() {
  runSU=$1
  eval "$SLAB_DIR/util/exec.sh $DEBUG $runSU 0 $2"
}

load_config() {

  DEBUG=$( sh $SLAB_DIR/util/conf_loader.sh DEBUG $SLAB_DIR/scriptlab.conf ) 
  
  MAKE_PATH=$( sh $SLAB_DIR/util/conf_loader.sh MAKE_PATH $WORK_DIR/norvid.conf )
  
  MCU=$( sh $SLAB_DIR/util/conf_loader.sh MCU $WORK_DIR/norvid.conf )
  
  GDB_ENABLE=$( sh $SLAB_DIR/util/conf_loader.sh GDB_ENABLE $WORK_DIR/norvid.conf )
  GDB_SERVER_CMD=$( sh $SLAB_DIR/util/conf_loader.sh GDB_SERVER_CMD $WORK_DIR/norvid.conf )
  GDB_CMD=$( sh $SLAB_DIR/util/conf_loader.sh GDB_CMD $WORK_DIR/norvid.conf )
}

check_work_dir() {
  call_dir=$(pwd)
  while [ $call_dir != "/" ]
  do
    if [ -e $call_dir/norvid.conf ] && [ -e $call_dir/main.c ]
    then
      WORK_DIR="$call_dir"
    fi
    call_dir=$(dirname "$call_dir")
  done

  if [ "$WORK_DIR" == "" ]
  then
    echo "You're not within a working directory!"
    exit 1
  fi
}

check_tmux_session() {
  search_depth=2
  pid=$$
  ppidName=""

  while [ $search_depth -gt 0 ] || [ "$ppidName" != "tmux: server" ]
  do

    stat=($(</proc/${pid}/stat))
    ppid=${stat[3]}
    ppidName=$( ps -p $ppid -o comm= )
   
    pid=${ppid}
    search_depth=$((search_depth-1))
  done

  if [ "$ppidName" == "tmux: server" ]
  then
    return 0
  else
    return 1
  fi
}

nv_usage() {
  echo "[ Usage ]"
  echo "norvid {init|build|flash|debug|code}"
  echo "nv {i|b|f|d|c}"
}

nv_init() {
  slab_exec 1 "sh $SLAB_DIR/util/init.sh norvid main.c"
}

nv_build() {
  slab_exec 1 "make -C $WORK_DIR/$MAKE_PATH"
}

nv_clean() {
  slab_exec 1 "make clean -C $WORK_DIR/$MAKE_PATH"
}

nv_flash() {
  slab_exec 1 "make flash -C $WORK_DIR/$MAKE_PATH"
}

nv_code() {

  if [ check_tmux_session ]
  then
    slab_exec 1 "tmux new-window 'vim $WORK_DIR/main.c'"
  else
    slab_exec 1 "tmux new-session 'vim $WORK_DIR/main.c'"
  fi

  [[ $GDB_ENABLE ]] && return
  # Pane 2: GDB server
  #slab_exec 1 "tmux split-window -p 30 '$GDB_SERVER_CMD'"
  slab_exec 1 "tmux split-window -p 30"

  # Pane 3: GDB server
  slab_exec 1 "tmux split-window -h '$GDB_CMD $WORK_DIR/$MAKE_PATH/${MCU}_xxaa.out'"

  slab_exec 1 "$GDB_SERVER_CMD -device '${MCU}'_xxAA"
}


#
# MAIN
#

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
SLAB_DIR="$SCRIPT_DIR/../.."


case $1 in

  init)
    nv_init
    ;;

  build|b)
    check_work_dir
    load_config
    nv_build
    ;;

  clean)
    check_work_dir
    load_config
    nv_clean
    ;;

  flash|f)
    check_work_dir
    load_config
    nv_flash
    ;;

  code)
    check_work_dir
    load_config
    nv_code
    ;;

  help|--help|*)
    nv_usage
    ;;

esac

